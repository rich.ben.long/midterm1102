class Component {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.visible = true;
  }
  getType() {
    return "unknown";
  }
  update() {}
}

class Bird extends Component {
  constructor(x, y) {
    super(x, y, 50, 50);
    this.status = 1;
  }
  getType() {
    return "Bird";
  }
  update() {
    if (this.visible&&this.status%2!=0) {
      ctx.clearRect(this.x, this.y, this.width, this.height);
      let bird1=document.getElementById("1");
      ctx.drawImage(bird1,0,0,419,265,this.x,this.y,50,50);
      this.status+=1;
      
      if(Math.random()>0.8){
        this.x = this.x+5;
        this.y = this.y-1;
        
    }
    
  }
    else if(this.visible&&this.status%2 == 0){
      ctx.clearRect(this.x, this.y, this.width, this.height);
      let bird2=document.getElementById("4");
      ctx.drawImage(bird2,0,0,419,265,this.x,this.y,50,50);
      this.status += 1;
      

      if(Math.random()>0.8){
        this.x = this.x+5;
        this.y = this.y+1;
    }
    
  }
    else if(!this.visible){
      let die = document.getElementById("5");
      ctx.drawImage(die,0,0,549,351,this.x,this.y,50,50);
      this.y= this.y+1;
    }
    
    }
  }


class Gun extends Component {
  constructor(x, y) {
    super(x, y, 50, 150);
  }
  getType() {
    return "Gun";
  }
  update() {
    if (this.visible) {
      let image = document.getElementById("gun");
      ctx.drawImage(image, 0, 0, 116, 403, this.x, this.y, 50, 150);
    }
  }
}

class Bullet extends Component {
  constructor(x, y, index) {
    super(x, y, 10, 40);
    this.index = index;
  }
  getType() {
    return "Bullet";
  }
  update() {
    if (this.index > bulletCount) {
      this.visible = false;
    }
    if (this.visible) {
      let image = document.getElementById("bullet");
      ctx.drawImage(image, 0, 0, 10, 40, this.x, this.y, 10, 40);
    }
  }
}

class Fire extends Component {
  constructor(x, y, index) {
    super(x, y, 10, 70);
    this.index = index;
  }
  getType() {
    return "Fire";
  }
  update() {
    if (this.visible) {
      let image = document.getElementById("fire");
      ctx.drawImage(image, 0, 0, 18, 129, this.x, this.y, 10, 70);
      this.y = this.y - 1;
    }
    else{
      ctx.clearRect(this.x, this.y, this.width, this.height);
    }
  }
}
